ARG BASE_IMAGE=uprev/base
ARG BASE_TAG=ubuntu-22.04
FROM ${BASE_IMAGE}:${BASE_TAG}

LABEL maintainer="JBerger@up-rev.com"
ENV DEBIAN_FRONTEND noninteractive

ARG IDE_VERSION=11.8.0_1165 
WORKDIR  /usr/src/mcuxpresso


# Install any needed packages specified in requirements.txt
RUN apt update && \
    apt-get full-upgrade -y && apt-get install -y software-properties-common &&\
    add-apt-repository ppa:deadsnakes/ppa && apt update && apt install -y \
    whiptail \
    libxrender1 \
    libxcb-render0 \
    libxcb-render-util0 \
    libxcb-shape0 \
    libxcb-randr0 \
    libxcb-xfixes0 \
    libxcb-sync1 \
    libxcb-shm0 \
    libxcb-icccm4 \
    libxcb-keysyms1 \
    libxcb-image0 \
    libxkbcommon0 \
    libxkbcommon-x11-0 \
    libfontconfig1 \
    libfreetype6 \
    libxext6 \
    libx11-6 \
    libxcb1 \
    libx11-xcb1 \
    libsm6 \
    libice6 \
    libglib2.0-0 \
    libncurses5 \
    udev \
    libusb-1.0-0-dev \
    dfu-util \
    libncursesw5 \
    libswt-gtk-4-java \
    libswt-gtk-4-jni \
    python3.8 \
    && apt-get clean -y && apt-get autoclean && rm -r /var/lib/apt/lists/* 


#Copy the mcuxpresso installer to the container
COPY mcuxpressoide-${IDE_VERSION}.x86_64.deb.bin /usr/src/mcuxpresso
    

#start ufevd and monitor so we can successfully update rules with jlink installation
RUN /lib/systemd/systemd-udevd --daemon &&\
  (udevadm monitor &) &&\
  #run the installer and accept the license
  chmod a+x mcuxpressoide-${IDE_VERSION}.x86_64.deb.bin &&\
  ./mcuxpressoide-${IDE_VERSION}.x86_64.deb.bin -- --acceptLicense


ENV TOOLCHAIN_PATH /usr/local/mcuxpressoide/ide/tools/bin
ENV IDE_PATH /usr/local/mcuxpressoide/ide
ENV PATH $IDE_PATH:$TOOLCHAIN_PATH:$PATH

#clean up installation files
RUN rm mcuxpressoide-${IDE_VERSION}.x86_64.deb.bin
RUN rm -rf mcu


