MCUXpresso Devcontainer
=======================


This repo contains the dockerfile and devcontainer configuration for `uprev/mcuxpresso <hub.docker.com/uprev/mxuxpresso>`_

This image contains the MCUXpresso IDE and toolchains
