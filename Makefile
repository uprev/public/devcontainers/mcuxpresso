#@file Makefile
#@brief makefile for containerized builds of project artifacts 
#@author Jason Berger
#@date 02/20/2023
  

#Project
PROJECT_NAME ?= hello-world
BUILD_CONFIG ?= Release
PROJ_DIR ?= .
DEVCONTAINER_IMAGE ?= uprev/mcuxpresso


.PHONY: docker-mount docker-build docker-doc native-build

#Default is a passthrough to call targets in the default makefile
.DEFAULT:
	docker run --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) \
	make $@

docker-mount:
	docker run -it --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) /bin/bash


native-build: 
	/usr/local/mcuxpressoide/ide/mcuxpressoide -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data /tmp/ws -import $(PROJ_DIR) -build $(PROJECT_NAME)/$(BUILD_CONFIG)

docker-build: 
	docker run --mount src=$(PWD),target=/workspace,type=bind -w /workspace $(DEVCONTAINER_IMAGE) \
	/usr/local/mcuxpressoide/ide/mcuxpressoide -nosplash -application org.eclipse.cdt.managedbuilder.core.headlessbuild -data /tmp/ws -import $(PROJ_DIR) -build $(PROJECT_NAME)/$(BUILD_CONFIG)

